-- mods/australia/biome_simpson_desert.lua
--[[
minetest.register_biome({
	name = "simpson_desert",
	--node_dust = "",
	node_top = "australia:red_sand",
	depth_top = 3,
	node_filler = "australia:red_stone",
	depth_filler = 2,
	--node_stone = "",
	--node_water_top = "",
	--depth_water_top = ,
	--node_water = "",
	node_river_water = "australia:muddy_river_water_source",
	y_min = 36,
	y_max = 31000,
	heat_point = 85,
	humidity_point = 10,
})



--
-- Register ores
--

-- All mapgens except singlenode
-- Blob ore first to avoid other ores inside blobs



--
-- Trees
--

-- Coolabah Tree
for _, schem in ipairs(aus.schematics.coolabah_tree) do
	minetest.register_decoration({
		deco_type = "schematic",
		sidelen = 80,
		place_on = {"australia:red_sand"},
		y_min = 36,
		y_max = 150,
		fill_ratio = (max_r-r+1)/20000,
		biomes = {"simpson_desert"},
		schematic = schem,
		flags = "place_center_x, place_center_z",
		rotation = "random",
	})
end

-- Desert Quandong
for _, schem in ipairs(aus.schematics.quandong_tree) do
	minetest.register_decoration({
		deco_type = "schematic",
		sidelen = 80,
		place_on = {"australia:red_sand"},
		y_min = 36,
		y_max = 130,
		fill_ratio = (max_r-r+1)/15000,
		biomes = {"simpson_desert"},
		schematic = schem,
		flags = "place_center_x, place_center_z",
		rotation = "random",
	})
end]]
