-- Southern Sassafras
aus.schematics.southern_sassafras_tree = {}
local max_r = 5
local fruit = nil
local tree = "australia:southern_sassafras_tree"
local leaves = "australia:southern_sassafras_leaves"
for r = 4,max_r do
	local schem = aus.generate_conifer_schematic(6, 4, tree, leaves, fruit)
	table.insert(aus.schematics.southern_sassafras_tree, schem)
end
