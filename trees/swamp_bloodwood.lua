-- Swamp Bloodwood
aus.schematics.swamp_bloodwood_tree = {}
local max_r = 4
local ht = 6
local fruit = nil
local limbs = nil
local tree = "australia:swamp_bloodwood_tree"
local leaves = "australia:swamp_bloodwood_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.swamp_bloodwood_tree, schem)
end
