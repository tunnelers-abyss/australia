-- Blue Gum
aus.schematics.blue_gum_tree = {}
local max_r = 7
local ht = 12
local fruit = nil
local limbs = nil
local tree = "australia:blue_gum_tree"
local leaves = "australia:blue_gum_leaves"
for r = 6,max_r do
	local schem = aus.generate_tree_schematic(10, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.blue_gum_tree, schem)
end
