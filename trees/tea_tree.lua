-- Tea Tree
aus.schematics.tea_tree_tree = {}
local max_r = 3
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:tea_tree_tree"
local leaves = "australia:tea_tree_leaves"
for r = 2,max_r do
	local schem = aus.generate_tree_schematic(1, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.tea_tree_tree, schem)
end
