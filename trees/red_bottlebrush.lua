-- Red Bottlebrush
aus.schematics.red_bottlebrush_tree = {}
local max_r = 3
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:red_bottlebrush_tree"
local leaves = "australia:red_bottlebrush_leaves"
for r = 2,max_r do
	local schem = aus.generate_tree_schematic(1, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.red_bottlebrush_tree, schem)
end
