-- Rottnest Island Pine
aus.schematics.rottnest_island_pine_tree = {}
local max_r = 3
local fruit = nil
local tree = "australia:rottnest_island_pine_tree"
local leaves = "australia:rottnest_island_pine_leaves"
for r = 2,max_r do
	local schem = aus.generate_conifer_schematic(3, 2, tree, leaves, fruit)
	table.insert(aus.schematics.rottnest_island_pine_tree, schem)
end
