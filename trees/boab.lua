-- Boab Tree
aus.schematics.boab_tree = {}
local max_r = 4
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:boab_tree"
local leaves = "australia:boab_leaves"
for r = 3,max_r do
	local schem = aus.generate_giant_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.boab_tree, schem)
end
