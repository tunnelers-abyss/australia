-- Wirewood
aus.schematics.wirewood_tree = {}
local max_r = 4
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:wirewood_tree"
local leaves = "australia:wirewood_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.wirewood_tree, schem)
end
