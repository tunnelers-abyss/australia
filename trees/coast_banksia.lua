-- Coast Banksia (big)
aus.schematics.coast_banksia_big_tree = {}
local max_r = 6
local ht = 7
local fruit = nil
local limbs = false
local tree = "australia:coast_banksia_tree"
local leaves = "australia:coast_banksia_leaves"
for r = 5,max_r do
	local schem = aus.generate_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.coast_banksia_big_tree, schem)
end

-- Coast Banksia (small)
aus.schematics.coast_banksia_small_tree = {}
local max_r = 4
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:coast_banksia_tree"
local leaves = "australia:coast_banksia_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(1, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.coast_banksia_small_tree, schem)
end

aus.schematics.coast_banksia_tree = {}
for _, schem in ipairs(aus.schematics.coast_banksia_big_tree) do
    table.insert(aus.schematics.coast_banksia_tree, schem)
end
for _, schem in ipairs(aus.schematics.coast_banksia_small_tree) do
    table.insert(aus.schematics.coast_banksia_tree, schem)
end