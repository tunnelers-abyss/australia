-- Paperbark Tree
aus.schematics.paperbark_tree = {}
local max_r = 5
local ht = 8
local fruit = nil
local limbs = nil
local tree = "australia:paperbark_tree"
local leaves = "australia:paperbark_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(4, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.paperbark_tree, schem)
end
