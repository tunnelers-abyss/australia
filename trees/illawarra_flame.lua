aus.schematics.illawarra_flame_tree = {}
local max_r = 6
local ht = 5
local fruit = nil
local limbs = false
local tree = "australia:illawarra_flame_tree"
local leaves = "australia:illawarra_flame_leaves"
for r = 5,max_r do
	local schem = aus.generate_tree_schematic(4, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.illawarra_flame_tree, schem)
end
