-- Swamp Gum
aus.schematics.swamp_gum_tree = {}
local max_r = 6
local ht = 18
local fruit = nil
local limbs = nil
local tree = "australia:swamp_gum_tree"
local leaves = "australia:swamp_gum_leaves"
for r = 6,max_r do
	local schem = aus.generate_giant_tree_schematic(26, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.swamp_gum_tree, schem)
end
