-- Merbau
aus.schematics.merbau_tree = {}
local max_ht = 12
local r = 8
local tree = "australia:merbau_tree"
local leaves = "australia:merbau_leaves"
for h = 8,max_ht do
	local schem = aus.generate_rainforest_tree_schematic(max_ht, r, tree, leaves)
	table.insert(aus.schematics.merbau_tree, schem)
end
