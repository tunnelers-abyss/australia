-- Mangrove Apple
aus.schematics.mangrove_apple_tree = {}
local max_r = 5
local ht = 4
local fruit = "australia:mangrove_apple"
local limbs = false
local tree = "australia:mangrove_apple_tree"
local leaves = "australia:mangrove_apple_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(1, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.mangrove_apple_tree, schem)
end
