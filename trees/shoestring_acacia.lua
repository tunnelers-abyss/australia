-- Shoestring Acacia
aus.schematics.shoestring_acacia_tree = {}
local max_r = 3
local ht = 5
local fruit = nil
local limbs = false
local tree = "australia:shoestring_acacia_tree"
local leaves = "australia:shoestring_acacia_leaves"
for r = 2,max_r do
	local schem = aus.generate_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.shoestring_acacia_tree, schem)
end
