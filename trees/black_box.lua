-- Black Box
aus.schematics.black_box_tree = {}
local max_r = 8
local ht = 8
local fruit = nil
local limbs = nil
local tree = "australia:black_box_tree"
local leaves = "australia:black_box_leaves"
for r = 6,max_r do
	local schem = aus.generate_big_tree_schematic(4, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.black_box_tree, schem)
end
