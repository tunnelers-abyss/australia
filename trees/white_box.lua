-- White Box
aus.schematics.white_box_tree = {}
local max_r = 8
local ht = 7
local fruit = nil
local limbs = nil
local tree = "australia:white_box_tree"
local leaves = "australia:white_box_leaves"
for r = 6,max_r do
	local schem = aus.generate_tree_schematic(5, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.white_box_tree, schem)
end
