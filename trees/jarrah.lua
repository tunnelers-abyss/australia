-- Jarrah
aus.schematics.jarrah_tree = {}
local max_r = 10
local ht = 12
local fruit = nil
local limbs = nil
local tree = "australia:jarrah_tree"
local leaves = "australia:jarrah_leaves"
for r = 8,max_r do
	local schem = aus.generate_giant_tree_schematic(8, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.jarrah_tree, schem)
end
