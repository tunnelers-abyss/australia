-- Grey Mangrove
aus.schematics.grey_mangrove_tree = {}
local max_ht = 6
local tree = "australia:grey_mangrove_tree"
local leaves = "australia:grey_mangrove_leaves"
for h = 4,max_ht do
	local schem = aus.generate_mangrove_tree_schematic(3, tree, leaves)
	table.insert(aus.schematics.grey_mangrove_tree, schem)
end
