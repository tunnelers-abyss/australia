-- Cloncurry Box
aus.schematics.cloncurry_box_tree = {}
local max_r = 6
local ht = 4
local fruit = nil
local limbs = nil
local tree = "australia:cloncurry_box_tree"
local leaves = "australia:cloncurry_box_leaves"
for r = 5,max_r do
	local schem = aus.generate_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.cloncurry_box_tree, schem)
end
