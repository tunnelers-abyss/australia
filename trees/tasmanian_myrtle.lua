-- Tasmanian Myrtle
aus.schematics.tasmanian_myrtle_tree = {}
local max_r = 8
local ht = 14
local fruit = nil
local limbs = nil
local tree = "australia:tasmanian_myrtle_tree"
local leaves = "australia:tasmanian_myrtle_leaves"
for r = 6,max_r do
	local schem = aus.generate_big_tree_schematic(6, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.tasmanian_myrtle_tree, schem)
end
