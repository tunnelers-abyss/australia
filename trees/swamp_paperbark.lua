-- Swamp Paperbark
aus.schematics.swamp_paperbark_tree = {}
local max_r = 5
local ht = 9
local fruit = nil
local limbs = nil
local tree = "australia:swamp_paperbark_tree"
local leaves = "australia:swamp_paperbark_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.swamp_paperbark_tree, schem)
end
