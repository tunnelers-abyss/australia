-- Snow Gum
aus.schematics.snow_gum_tree = {}
local max_r = 4
local ht = 4
local fruit = nil
local limbs = nil
local tree = "australia:snow_gum_tree"
local leaves = "australia:snow_gum_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(1, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.snow_gum_tree, schem)
end
