-- Marri
aus.schematics.marri_tree = {}
local max_r = 10
local ht = 12
local fruit = nil
local limbs = nil
local tree = "australia:marri_tree"
local leaves = "australia:marri_leaves"
for r = 8,max_r do
	local schem = aus.generate_big_tree_schematic(8, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.marri_tree, schem)
end
