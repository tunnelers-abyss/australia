-- Mulga Tree
aus.schematics.mulga_tree = {}
local max_r = 4
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:mulga_tree"
local leaves = "australia:mulga_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.mulga_tree, schem)
end
