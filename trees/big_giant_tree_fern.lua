-- Giant tree fern sapling
minetest.register_node("australia:sapling_giant_tree_fern", {
	description = "Dicksonia Antarctica: Giant Tree Fern Sapling",
	drawtype = "plantlike",
	paramtype = "light",
	tiles = {"aus_sapling_tree_fern_giant.png"},
	inventory_image = "aus_sapling_tree_fern_giant.png",
	walkable = false,
	groups = {snappy=3,flammable=2,attached_node=1,sapling=1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-7/16, -1/2, -7/16, 7/16, 0, 7/16},
	},
})

-- Giant tree fern leaves
minetest.register_node("australia:tree_fern_leaves_giant", {
	description = "Dicksonia Antarctica: Giant Tree Fern Crown",
	drawtype = "mesh",
	mesh = "big_giant_tree_fern_crown.obj",
	paramtype = "light",
	tiles = {"aus_fern_tree_giant.png"},
	inventory_image = "aus_fern_tree.png",
	use_texture_alpha = "clip",
	walkable = false,
	groups = {
		snappy=3,
		flammable=2,
		leaves=1
	},
	drop = {
		max_items = 1,
		items = {
			{
				items = {"australia:sapling_giant_tree_fern 2"},
				rarity = 10,
			},
			{
				items = {"australia:tree_fern_leaves_giant"},
			}
		}
	},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-1.5, -0.5, -1.5, 1.5, 0.2, 1.5},
	},
})

	-- Giant tree fern trunk
minetest.register_node("australia:fern_trunk_big", {
	description = "Dicksonia Antarctica: Giant Tree Fern Trunk",
	drawtype = "nodebox",
	paramtype = "light",
	tiles = {
		"aus_fern_trunk_big_top.png",
		"aus_fern_trunk_big_top.png",
		"aus_fern_trunk_big.png"
	},
	use_texture_alpha = "clip",
	node_box = {
		type = "fixed",
		fixed = {-1/4, -1/2, -1/4, 1/4, 1/2, 1/4},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/4, -1/2, -1/4, 1/4, 1/2, 1/4},
	},
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=2,flammable=3,wood=1},
	sounds = default.node_sound_wood_defaults(),
	after_destruct = function(pos,oldnode)
		local node = minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})
		if node.name == "australia:fern_trunk_big" or node.name == "australia:fern_trunk_big_top" then
			minetest.dig_node({x=pos.x,y=pos.y+1,z=pos.z})
			minetest.add_item(pos,"australia:fern_trunk_big")
		end
	end,
})

minetest.register_alias("australia:tree_fern_leaf_big", "air")
minetest.register_alias("australia:tree_fern_leaf_big_end", "air")
minetest.register_alias("australia:fern_trunk_big_top", "australia:fern_trunk_big")

aus.schematics.big_tree_fern = {
	{
		size = { x = 1, y = 6, z = 1},
		data = {
			{ name = "ignore", param1 = 0, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:tree_fern_leaves_giant", param1 = 255, param2 = 0 },
		},
	}
}

aus.schematics.giant_tree_fern = {
	{
		size = { x = 1, y = 11, z = 1},
		data = {
			{ name = "ignore", param1 = 0, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:fern_trunk_big", param1 = 255, param2 = 0 },
			{ name = "australia:tree_fern_leaves_giant", param1 = 255, param2 = 0 },
		},
	}
}

aus.schematics.big_giant_tree_fern = {}
for _, schem in ipairs(aus.schematics.big_tree_fern) do
	table.insert(aus.schematics.big_giant_tree_fern, schem)
end
for _, schem in ipairs(aus.schematics.giant_tree_fern) do
	table.insert(aus.schematics.big_giant_tree_fern, schem)
end