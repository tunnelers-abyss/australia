-- Lemon Myrtle
aus.schematics.lemon_myrtle_tree = {}
local max_r = 3
local ht = 4
local fruit = nil
local limbs = false
local tree = "australia:lemon_myrtle_tree"
local leaves = "australia:lemon_myrtle_leaves"
for r = 2,max_r do
	local schem = aus.generate_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.lemon_myrtle_tree, schem)
end
