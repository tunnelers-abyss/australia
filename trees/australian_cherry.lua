-- Australian Cherry
aus.schematics.cherry_tree = {}
local max_r = 3
local fruit = "australia:cherry"
local tree = "australia:cherry_tree"
local leaves = "australia:cherry_leaves"
for r = 2,max_r do
	local schem = aus.generate_conifer_schematic(3, 2, tree, leaves, fruit)
	table.insert(aus.schematics.cherry_tree, schem)
end
