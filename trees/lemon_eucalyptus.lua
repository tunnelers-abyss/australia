-- Lemon Eucalyptus
aus.schematics.lemon_eucalyptus_tree = {}
local max_r = 5
local ht = 7
local fruit = nil
local limbs = nil
local tree = "australia:lemon_eucalyptus_tree"
local leaves = "australia:lemon_eucalyptus_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(8, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.lemon_eucalyptus_tree, schem)
end
