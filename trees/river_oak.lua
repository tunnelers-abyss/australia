local max_r, ht, fruit, limbs, tree, leaves

-- River Oak
aus.schematics.river_oak_big_tree = {}
max_r = 5
ht = 9
fruit = nil
limbs = false
tree = "australia:river_oak_tree"
leaves = "australia:river_oak_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(3, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.river_oak_big_tree, schem)
end

aus.schematics.river_oak_small_tree = {}
max_r = 4
ht = 6
fruit = nil
limbs = false
tree = "australia:river_oak_tree"
leaves = "australia:river_oak_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.river_oak_small_tree, schem)
end

aus.schematics.river_oak_tree = {}
for _, schem in ipairs(aus.schematics.river_oak_big_tree) do
	table.insert(aus.schematics.river_oak_tree, schem)
end
for _, schem in ipairs(aus.schematics.river_oak_small_tree) do
	table.insert(aus.schematics.river_oak_tree, schem)
end
