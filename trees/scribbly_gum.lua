-- Scribbly Gum
aus.schematics.scribbly_gum_tree = {}
local max_r = 5
local ht = 5
local fruit = nil
local limbs = nil
local tree = "australia:scribbly_gum_tree"
local leaves = "australia:scribbly_gum_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(4, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.scribbly_gum_tree, schem)
end
