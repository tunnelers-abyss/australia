-- Karri
aus.schematics.karri_tree = {}
local max_r = 12
local ht = 10
local fruit = nil
local limbs = nil
local tree = "australia:karri_tree"
local leaves = "australia:karri_leaves"
for r = 10,max_r do
	local schem = aus.generate_giant_tree_schematic(20, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.karri_tree, schem)
end
