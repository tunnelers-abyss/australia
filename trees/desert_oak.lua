-- Desert Oak
aus.schematics.desert_oak_tree = {}
local max_r = 8
local ht = 5
local fruit = nil
local limbs = nil
local tree = "australia:desert_oak_tree"
local leaves = "australia:desert_oak_leaves"
for r = 6,max_r do
	local schem = aus.generate_tree_schematic(6, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.desert_oak_tree, schem)
end