-- Sugar Gum
aus.schematics.sugar_gum_tree = {}
local max_r = 11
local ht = 10
local fruit = nil
local limbs = nil
local tree = "australia:sugar_gum_tree"
local leaves = "australia:sugar_gum_leaves"
for r = 9,max_r do
	local schem = aus.generate_tree_schematic(8, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.sugar_gum_tree, schem)
end
