-- Huon Pine
aus.schematics.huon_pine_tree = {}
local max_r = 4
local ht = 8
local fruit = nil
local limbs = nil
local tree = "australia:huon_pine_tree"
local leaves = "australia:huon_pine_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(4, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.huon_pine_tree, schem)
end
