-- Daintree Stringybark
aus.schematics.daintree_stringybark_tree = {}
local max_r = 7
local ht = 10
local fruit = nil
local limbs = nil
local tree = "australia:daintree_stringybark_tree"
local leaves = "australia:daintree_stringybark_leaves"
for r = 6,max_r do
	local schem = aus.generate_tree_schematic(9, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.daintree_stringybark_tree, schem)
end
