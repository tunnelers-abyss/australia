-- Moreton Bay Fig
aus.schematics.moreton_bay_fig_tree = {}
local max_r = 13
local ht = 12
local fruit = "australia:moreton_bay_fig"
local limbs = nil
local tree = "australia:moreton_bay_fig_tree"
local leaves = "australia:moreton_bay_fig_leaves"
for r = 11,max_r do
	local schem = aus.generate_giant_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.moreton_bay_fig_tree, schem)
end
