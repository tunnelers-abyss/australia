-- Macadamia Tree
aus.schematics.macadamia_tree = {}
local max_r = 6
local ht = 6
local fruit = "australia:macadamia"
local limbs = nil
local tree = "australia:macadamia_tree"
local leaves = "australia:macadamia_leaves"
for r = 4,max_r do
	local schem = aus.generate_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.macadamia_tree, schem)
end
