aus.schematics.celery_top_pine_tree = {}
local max_r = 5
local fruit = nil
local tree = "australia:celery_top_pine_tree"
local leaves = "australia:celery_top_pine_leaves"
for r = 4,max_r do
	local schem = aus.generate_conifer_schematic(5, 4, tree, leaves, fruit)
	table.insert(aus.schematics.celery_top_pine_tree, schem)
end
