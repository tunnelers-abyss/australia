-- Australian Fan Palm
aus.schematics.fan_palm_tree = {}
local max_ht = 9
local r = 3
local tree = "australia:fan_palm_tree"
local leaves = "australia:fan_palm_leaves"
for h = 9,max_ht do
	local schem = aus.generate_fanpalm_tree_schematic(max_ht, r, tree, leaves)
	table.insert(aus.schematics.fan_palm_tree, schem)
end
