-- Bull Banksia
aus.schematics.bull_banksia_tree = {}
local max_r = 4
local ht = 5
local fruit = nil
local limbs = false
local tree = "australia:bull_banksia_tree"
local leaves = "australia:bull_banksia_leaves"
for r = 3,max_r do
	local schem = aus.generate_tree_schematic(2, {x=r, y=ht, z=r}, tree, leaves, fruit, limbs)
	table.insert(aus.schematics.bull_banksia_tree, schem)
end
